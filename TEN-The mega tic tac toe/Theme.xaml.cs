﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace TEN_The_mega_tic_tac_toe
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Theme : Page
    {
        int selectedPage, selectedTheme, currentPage;
        double widthFector, heightFector, windowWidth, windowHeight;
        Color[] colors;
        ComponentMaintainer cm;
        Rectangle[,] Theme_color;
        Ellipse[] page_indicator;
        Image[] selected_theme_img;
        StorageFile user_preferences;

        public Theme()
        {
            this.InitializeComponent();
            colors = new Color[3];
            getPreferenceFile();
            cm = new ComponentMaintainer(50000);
            widthFector = cm.widthFector;
            heightFector = cm.heightFector;
            maintainComponent();
            if (cm.transformComponents)
                transformToPortraitMode();
            Window.Current.SizeChanged += Current_SizeChanged;
            generateThemes();
        }

        void maintainComponent()
        {
            cm.maintainShape(title_back);
            cm.maintainShape(theme_back);
            cm.maintainImage(next_page);
            cm.maintainImage(pre_page);
            cm.maintainImage(back_button);
            cm.maintainTextblock(title);
        }

        void Current_SizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            switch (ApplicationView.Value)
            {
                case ApplicationViewState.FullScreenLandscape:
                    transformToLandscapeMode();
                    break;
                case ApplicationViewState.FullScreenPortrait:
                    transformToPortraitMode();
                    break;
            }
        }

        void transformToPortraitMode()
        {
            windowWidth = Window.Current.Bounds.Width;
            windowHeight = Window.Current.Bounds.Height;

            pre_page.Height = 45 * heightFector;
            pre_page.Width = 50 * widthFector;
            next_page.Height = 45 * heightFector;
            next_page.Width = 50 * widthFector;

            theme_back.Width = windowWidth;
            title_back.Width = windowWidth;
            theme_back.Height = windowHeight - title_back.Height;

            cm.setTextBlock(title, (windowWidth - title.Width) / 2, title.Margin.Top);
            cm.setImage(pre_page, theme_back.Width / 2 - 2 * pre_page.Width, title_back.Height + theme_back.Height / 2 + 278 * heightFector);
            cm.setImage(next_page, theme_back.Width / 2 + next_page.Width, pre_page.Margin.Top);
        }

        void transformToLandscapeMode()
        {
            windowWidth = Window.Current.Bounds.Width;
            windowHeight = Window.Current.Bounds.Height;

            pre_page.Height = 320 * heightFector;
            pre_page.Width = 100 * widthFector;
            next_page.Height = 320 * heightFector;
            next_page.Width = 100 * widthFector;

            theme_back.Width = windowWidth;
            title_back.Width = windowWidth;
            theme_back.Height = windowHeight - title_back.Height;

            cm.setTextBlock(title, (windowWidth - title.Width) / 2, title.Margin.Top);
            cm.setImage(pre_page, theme_back.Width / 2 - 300 * widthFector - 2 * pre_page.Width, title_back.Height + (theme_back.Height - pre_page.Height) / 2);
            cm.setImage(next_page, theme_back.Width / 2 + 300 * widthFector + next_page.Width, pre_page.Margin.Top);
        }

        void generateThemes()
        {
            Theme_color = new Rectangle[5, 3];
            selected_theme_img = new Image[5];
            page_indicator = new Ellipse[2];
            for (byte i = 0; i < 5; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Theme_color[i, j] = new Rectangle();
                    Theme_color[i, j].Width = 200 * widthFector;
                    Theme_color[i, j].Height = 100 * heightFector;
                    Theme_color[i, j].Margin = new Thickness((-400 + (j * 400)) * widthFector, (-430 + (i * 240)) * heightFector, 0, 0);
                    myGrid.Children.Add(Theme_color[i, j]);
                }
                selected_theme_img[i] = new Image();
                selected_theme_img[i].Width = 600 * widthFector;
                selected_theme_img[i].Height = 100 * heightFector;
                selected_theme_img[i].Margin = new Thickness(0, (-430 + (i * 240)) * heightFector, 0, 0);
                selected_theme_img[i].Source = new BitmapImage(new Uri(@"ms-appx:///images/blank.png", UriKind.RelativeOrAbsolute));
                selected_theme_img[i].Tag = (i + 1).ToString();
                selected_theme_img[i].PointerPressed += change_theme_clicked;
                myGrid.Children.Add(selected_theme_img[i]);
            }
            for (int i = 0; i < 2; i++)
            {
                page_indicator[i] = new Ellipse();
                page_indicator[i].Width = 10 * widthFector;
                page_indicator[i].Height = 10 * heightFector;
                page_indicator[i].Margin = new Thickness((-70 + (i * 140)) * widthFector, 680 * heightFector, 0, 0);
                myGrid.Children.Add(page_indicator[i]);
            }
        }

        void setSelectedTheme()
        {
            switch (selectedTheme)
            {
                case 1: selected_theme_img[0].Source = new BitmapImage(new Uri(@"ms-appx:///images/selected_theme.png", UriKind.RelativeOrAbsolute)); break;
                case 2: selected_theme_img[1].Source = new BitmapImage(new Uri(@"ms-appx:///images/selected_theme.png", UriKind.RelativeOrAbsolute)); break;
                case 3: selected_theme_img[2].Source = new BitmapImage(new Uri(@"ms-appx:///images/selected_theme.png", UriKind.RelativeOrAbsolute)); break;
                case 4: selected_theme_img[3].Source = new BitmapImage(new Uri(@"ms-appx:///images/selected_theme.png", UriKind.RelativeOrAbsolute)); break;
                case 5: selected_theme_img[4].Source = new BitmapImage(new Uri(@"ms-appx:///images/selected_theme.png", UriKind.RelativeOrAbsolute)); break;
            }
        }

        void resetSelectedTheme()
        {
            switch (selectedTheme)
            {
                case 1: selected_theme_img[0].Source = new BitmapImage(new Uri(@"ms-appx:///images/blank.png", UriKind.RelativeOrAbsolute)); break;
                case 2: selected_theme_img[1].Source = new BitmapImage(new Uri(@"ms-appx:///images/blank.png", UriKind.RelativeOrAbsolute)); break;
                case 3: selected_theme_img[2].Source = new BitmapImage(new Uri(@"ms-appx:///images/blank.png", UriKind.RelativeOrAbsolute)); break;
                case 4: selected_theme_img[3].Source = new BitmapImage(new Uri(@"ms-appx:///images/blank.png", UriKind.RelativeOrAbsolute)); break;
                case 5: selected_theme_img[4].Source = new BitmapImage(new Uri(@"ms-appx:///images/blank.png", UriKind.RelativeOrAbsolute)); break;
            }
        }

        void change_theme_clicked(object sender, PointerRoutedEventArgs e)
        {
            if (!(selectedPage == currentPage && selectedTheme == int.Parse(((Image)sender).Tag.ToString())))
            {
                selectedPage = currentPage;
                resetSelectedTheme();
                selectedTheme = int.Parse(((Image)sender).Tag.ToString());
                setSelectedTheme();
                setColorsArray();
                writeDataInFile();
            }
        }

        async void getPreferenceFile()
        {
            user_preferences = await ApplicationData.Current.LocalFolder.GetFileAsync("preferences.txt");
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            int[] data = e.Parameter as int[];
            selectedPage = data[0];
            selectedTheme = data[1];
            currentPage = selectedPage;
            if (selectedPage == 2)
            {
                next_page.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                pre_page.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            setCurrentPage();
            setSelectedTheme();
            setColorsArray();
        }

        void setCurrentPage()
        {
            switch (currentPage)
            {
                case 1:
                    page_indicator[0].Fill = new SolidColorBrush(Color.FromArgb(255, 61, 145, 165));
                    page_indicator[1].Fill = new SolidColorBrush(Color.FromArgb(255, 204, 202, 198));
                    page_indicator[0].Width = 15 * widthFector;
                    page_indicator[0].Height = 15 * heightFector;
                    page_indicator[1].Width = 10 * widthFector;
                    page_indicator[1].Height = 10 * heightFector;

                    Theme_color[0, 0].Fill = new SolidColorBrush(Color.FromArgb(255, 204, 202, 198));
                    Theme_color[0, 1].Fill = new SolidColorBrush(Color.FromArgb(255, 252, 118, 99));
                    Theme_color[0, 2].Fill = new SolidColorBrush(Color.FromArgb(255, 63, 103, 168));

                    Theme_color[1, 0].Fill = new SolidColorBrush(Color.FromArgb(255, 50, 52, 56));
                    Theme_color[1, 1].Fill = new SolidColorBrush(Color.FromArgb(255, 61, 145, 165));
                    Theme_color[1, 2].Fill = new SolidColorBrush(Color.FromArgb(255, 249, 99, 141));

                    Theme_color[2, 0].Fill = new SolidColorBrush(Color.FromArgb(255, 73, 66, 67));
                    Theme_color[2, 1].Fill = new SolidColorBrush(Color.FromArgb(255, 255, 115, 109));
                    Theme_color[2, 2].Fill = new SolidColorBrush(Color.FromArgb(255, 211, 203, 114));

                    Theme_color[3, 0].Fill = new SolidColorBrush(Color.FromArgb(255, 60, 60, 112));
                    Theme_color[3, 1].Fill = new SolidColorBrush(Color.FromArgb(255, 69, 147, 247));
                    Theme_color[3, 2].Fill = new SolidColorBrush(Color.FromArgb(255, 249, 199, 92));

                    Theme_color[4, 0].Fill = new SolidColorBrush(Color.FromArgb(255, 204, 203, 190));
                    Theme_color[4, 1].Fill = new SolidColorBrush(Color.FromArgb(255, 250, 195, 96));
                    Theme_color[4, 2].Fill = new SolidColorBrush(Color.FromArgb(255, 142, 169, 194));
                    break;
                case 2:
                    page_indicator[0].Fill = new SolidColorBrush(Color.FromArgb(255, 204, 202, 198));
                    page_indicator[1].Fill = new SolidColorBrush(Color.FromArgb(255, 61, 145, 165));
                    page_indicator[0].Width = 10 * widthFector;
                    page_indicator[0].Height = 10 * heightFector;
                    page_indicator[1].Width = 15 * widthFector;
                    page_indicator[1].Height = 15 * heightFector;

                    Theme_color[0, 0].Fill = new SolidColorBrush(Color.FromArgb(255, 0, 28, 48));
                    Theme_color[0, 1].Fill = new SolidColorBrush(Color.FromArgb(255, 157, 55, 105));
                    Theme_color[0, 2].Fill = new SolidColorBrush(Color.FromArgb(255, 66, 69, 119));

                    Theme_color[1, 0].Fill = new SolidColorBrush(Color.FromArgb(255, 63, 60, 62));
                    Theme_color[1, 1].Fill = new SolidColorBrush(Color.FromArgb(255, 178, 137, 70));
                    Theme_color[1, 2].Fill = new SolidColorBrush(Color.FromArgb(255, 140, 139, 140));

                    Theme_color[2, 0].Fill = new SolidColorBrush(Color.FromArgb(255, 53, 53, 62));
                    Theme_color[2, 1].Fill = new SolidColorBrush(Color.FromArgb(255, 134, 134, 137));
                    Theme_color[2, 2].Fill = new SolidColorBrush(Color.FromArgb(255, 108, 108, 111));

                    Theme_color[3, 0].Fill = new SolidColorBrush(Color.FromArgb(255, 53, 53, 62));
                    Theme_color[3, 1].Fill = new SolidColorBrush(Color.FromArgb(255, 255, 156, 194));
                    Theme_color[3, 2].Fill = new SolidColorBrush(Color.FromArgb(255, 255, 109, 58));

                    Theme_color[4, 0].Fill = new SolidColorBrush(Color.FromArgb(255, 224, 223, 207));
                    Theme_color[4, 1].Fill = new SolidColorBrush(Color.FromArgb(255, 250, 195, 96));
                    Theme_color[4, 2].Fill = new SolidColorBrush(Color.FromArgb(255, 167, 214, 191));
                    break;
            }
        }

        async void writeDataInFile()
        {
            string data = selectedPage + "$$"
                        + selectedTheme + "$$"
                        + colors[0].A + "$" + colors[0].R + "$" + colors[0].G + "$" + colors[0].B + "$$"
                        + colors[1].A + "$" + colors[1].R + "$" + colors[1].G + "$" + colors[1].B + "$$"
                        + colors[2].A + "$" + colors[2].R + "$" + colors[2].G + "$" + colors[2].B + "$$";
            await FileIO.WriteTextAsync(user_preferences, data);
        }

        void setColorsArray()
        {
            old_color.Value = ((SolidColorBrush)title_back.Fill).Color;
            colors[0] = ((SolidColorBrush)Theme_color[selectedTheme - 1, 0].Fill).Color;
            colors[1] = ((SolidColorBrush)Theme_color[selectedTheme - 1, 1].Fill).Color;
            colors[2] = ((SolidColorBrush)Theme_color[selectedTheme - 1, 2].Fill).Color;
            new_color.Value = colors[1];
            change_color.Begin();
            change_color.Completed += change_color_Completed;
        }

        void change_color_Completed(object sender, object e)
        {
            title_back.Fill = new SolidColorBrush(colors[1]);
        }

        void BackButton_pressed(object sender, PointerRoutedEventArgs e)
        {
            object[] data = new object[5];
            data[0] = selectedPage;
            data[1] = selectedTheme;
            data[2] = colors[0];
            data[3] = colors[1];
            data[4] = colors[2];
            this.Frame.Navigate(typeof(MainPage), data);
        }

        void change_page_clicked(object sender, PointerRoutedEventArgs e)
        {
            if (sender.Equals(next_page))
            {
                currentPage = 2;
                next_page.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                pre_page.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            else
            {
                currentPage = 1;
                next_page.Visibility = Windows.UI.Xaml.Visibility.Visible;
                pre_page.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }
            setCurrentPage();

            if (selectedPage != currentPage)
                resetSelectedTheme();
            else
                setSelectedTheme();
        }
    }
}
