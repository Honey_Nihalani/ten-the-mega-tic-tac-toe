﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace TEN_The_mega_tic_tac_toe
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AboutGame : Page
    {
        object[] data;
        double widthFector, heightFector, windowWidth, windowHeight;
        ComponentMaintainer cm;
        int current_page = 1;
        Ellipse[] page_indicator;
        Brush currentPageIndicate, otherPageIndicate;

        public AboutGame()
        {
            this.InitializeComponent();
            currentPageIndicate = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
            otherPageIndicate = new SolidColorBrush(Color.FromArgb(255, 110, 110, 110));
            cm = new ComponentMaintainer(75000);
            widthFector = cm.widthFector;
            heightFector = cm.heightFector;
            maintainComponent();
            generatePageIndicators();
            if (cm.transformComponents)
                transformToPortraitMode();
            Window.Current.SizeChanged += Current_SizeChanged;
        }

        void maintainComponent()
        {
            cm.maintainImage(back_button);
            cm.maintainImage(screen_shot);
            cm.maintainImage(pre_page);
            cm.maintainImage(next_page);
            cm.maintainTextblock(Description);
        }

        void generatePageIndicators()
        {
            page_indicator = new Ellipse[11];
            for (int i = 0; i < 11; i++)
            {
                page_indicator[i] = new Ellipse();
                page_indicator[i].Width = 10 * widthFector;
                page_indicator[i].Height = 10 * heightFector;
                page_indicator[i].Fill = new SolidColorBrush(Color.FromArgb(255, 110, 110, 110));
                page_indicator[i].Margin = new Thickness((-400 + (i * 80)) * widthFector, 720 * heightFector, 0, 0);
                myGrid.Children.Add(page_indicator[i]);
            }
        }

        void Current_SizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            switch (ApplicationView.Value)
            {
                case ApplicationViewState.FullScreenLandscape:
                    transformToLandscapeMode();
                    break;
                case ApplicationViewState.FullScreenPortrait:
                    transformToPortraitMode();
                    break;
            }
        }

        void transformToPortraitMode()
        {
            windowWidth = Window.Current.Bounds.Width;
            windowHeight = Window.Current.Bounds.Height;

            screen_shot.Width = 600 * widthFector;
            screen_shot.Height = 336 * heightFector;
            Description.Width = 620 * widthFector;
            Description.Height = 290 * heightFector;

            next_page.Width = 60 * widthFector;
            next_page.Height = 60 * heightFector;
            pre_page.Width = 60 * widthFector;
            pre_page.Height = 60 * heightFector;

            cm.setImage(screen_shot, (windowWidth - screen_shot.Width) / 2, (windowHeight - screen_shot.Height - Description.Height) / 2);
            cm.setTextBlock(Description, (windowWidth - Description.Width) / 2, screen_shot.Margin.Top + screen_shot.Height);
            cm.setImage(pre_page, windowWidth / 2 - 200 * widthFector - pre_page.Width, Description.Margin.Top + Description.Height + 14 * heightFector);
            cm.setImage(next_page, windowWidth / 2 + 200 * widthFector, pre_page.Margin.Top);
        }

        void transformToLandscapeMode()
        {
            windowWidth = Window.Current.Bounds.Width;
            windowHeight = Window.Current.Bounds.Height;

            screen_shot.Width = 1000 * widthFector;
            screen_shot.Height = 560 * heightFector;
            Description.Width = 1200 * widthFector;
            Description.Height = 150 * heightFector;

            next_page.Width = 100 * widthFector;
            next_page.Height = 200 * heightFector;
            pre_page.Width = 100 * widthFector;
            pre_page.Height = 200 * heightFector;

            cm.setImage(screen_shot, (windowWidth - screen_shot.Width) / 2, (windowHeight - screen_shot.Height - Description.Height) / 2);
            cm.setTextBlock(Description, (windowWidth - Description.Width) / 2, screen_shot.Margin.Top + screen_shot.Height);
            cm.setImage(pre_page, (screen_shot.Margin.Left - pre_page.Width) / 2, (windowHeight - pre_page.Height) / 2);
            cm.setImage(next_page, 3 * pre_page.Margin.Left + pre_page.Width + screen_shot.Width, pre_page.Margin.Top);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            data = e.Parameter as object[];
            setPageIndicator(current_page);
        }

        void BackButton_pressed(object sender, PointerRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage), data);
        }

        void changepage_pressed(object sender, PointerRoutedEventArgs e)
        {
            if (sender.Equals(next_page))
            {
                if (current_page < 11)
                    current_page++;
            }
            else
            {
                if (current_page > 1)
                    current_page--;
            }
            show_page();
        }

        void setPageIndicator(int i)
        {
            page_indicator[i - 1].Fill = currentPageIndicate;
            page_indicator[i - 1].Width = 15 * widthFector;
            page_indicator[i - 1].Height = 15 * heightFector;
            if (i == 1)
            {
                page_indicator[i].Fill = otherPageIndicate;
                page_indicator[i].Width = 10 * widthFector;
                page_indicator[i].Height = 10 * heightFector;
            }
            else if (i == 11)
            {
                page_indicator[i - 2].Fill = otherPageIndicate;
                page_indicator[i - 2].Width = 10 * widthFector;
                page_indicator[i - 2].Height = 10 * heightFector;
            }
            else
            {
                page_indicator[i].Fill = otherPageIndicate;
                page_indicator[i - 2].Fill = otherPageIndicate;
                page_indicator[i].Width = 10 * widthFector;
                page_indicator[i].Height = 10 * heightFector;
                page_indicator[i - 2].Width = 10 * widthFector;
                page_indicator[i - 2].Height = 10 * heightFector;
            }
        }

        void show_page()
        {
            setPageIndicator(current_page);
            switch (current_page)
            {
                case 1:
                    pre_page.Visibility = Visibility.Collapsed;
                    screen_shot.Source = new BitmapImage(new Uri(@"ms-appx:///images/About_game/Screenshot_1.png", UriKind.RelativeOrAbsolute));
                    Description.Text = "This is an enhanced version of famous Tic-tac-toe game with some interesting rules. Consider the rows and columns of simple Tic-tac-toe game correspond to the rows and columns of Tic-tac-toe zones.";
                    break;
                case 2:
                    pre_page.Visibility = Visibility.Visible;
                    screen_shot.Source = new BitmapImage(new Uri(@"ms-appx:///images/About_game/Screenshot_2.png", UriKind.RelativeOrAbsolute));
                    Description.Text = "Initially all the positions are enabled for first player. For opponent the zone that corresponds to the row and column marked by first player will be enabled and can mark any position in that zone.";
                    break;
                case 3:
                    screen_shot.Source = new BitmapImage(new Uri(@"ms-appx:///images/About_game/Screenshot_3.png", UriKind.RelativeOrAbsolute));
                    Description.Text = "Similarly, for first player the zone will be enabled corresponding to the row and column marked by second player in his turn.";
                    break;
                case 4:
                    screen_shot.Source = new BitmapImage(new Uri(@"ms-appx:///images/About_game/Screenshot_4.png", UriKind.RelativeOrAbsolute));
                    Description.Text = "Suppose a player succeeds to complete a trio in a particular zone."
                               + "\n" + "Than that zone will be marked as a big square indicating that the player has won that zone (for other player it will be circle).";
                    break;
                case 5:
                    screen_shot.Source = new BitmapImage(new Uri(@"ms-appx:///images/About_game/Screenshot_5.png", UriKind.RelativeOrAbsolute));
                    Description.Text = "If a player selects any position that has corresponding zone is either won by any player or draw then...";
                    break;
                case 6:
                    screen_shot.Source = new BitmapImage(new Uri(@"ms-appx:///images/About_game/Screenshot_6.png", UriKind.RelativeOrAbsolute));
                    Description.Text = "All the remaining zones will be enabled and the opponent will be able to select any position amongst them.";
                    break;
                case 7:
                    screen_shot.Source = new BitmapImage(new Uri(@"ms-appx:///images/About_game/Screenshot_7.png", UriKind.RelativeOrAbsolute));
                    Description.Text = "HINT: Your strategy should be such that your opponent never gets a chance to win zones in a trio fashion.";
                    break;
                case 8:
                    screen_shot.Source = new BitmapImage(new Uri(@"ms-appx:///images/About_game/Screenshot_8.png", UriKind.RelativeOrAbsolute));
                    Description.Text = "In a particular zone if neither of the two players completes a trio which means it’s a draw then...";
                    break;
                case 9:
                    screen_shot.Source = new BitmapImage(new Uri(@"ms-appx:///images/About_game/Screenshot_9.png", UriKind.RelativeOrAbsolute));
                    Description.Text = "That particular zone will be marked with a rectangle.";
                    break;
                case 10:
                    next_page.Visibility = Visibility.Visible;
                    screen_shot.Source = new BitmapImage(new Uri(@"ms-appx:///images/About_game/Screenshot_10.png", UriKind.RelativeOrAbsolute));
                    Description.Text = "If a player wins any three zones in horizontal, vertical or cross manner then he/she wins the game.";
                    break;
                case 11:
                    next_page.Visibility = Visibility.Collapsed;
                    screen_shot.Source = new BitmapImage(new Uri(@"ms-appx:///images/About_game/Screenshot_11.png", UriKind.RelativeOrAbsolute));
                    Description.Text = "At any time in game you get options in the option pane.";
                    break;
            }
        }
    }
}
