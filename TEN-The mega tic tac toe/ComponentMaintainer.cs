﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Shapes;

namespace TEN_The_mega_tic_tac_toe
{
    class ComponentMaintainer
    {
        Thickness margin;
        public bool transformComponents;
        public double widthFector, heightFector, fontDiff, windowWidth, windowHeight;

        public ComponentMaintainer(double font)
        {
            windowWidth = Window.Current.Bounds.Width;
            windowHeight = Window.Current.Bounds.Height;

            if (ApplicationView.Value == ApplicationViewState.FullScreenPortrait)
            {
                if (windowHeight / windowWidth < 1.3)
                {
                    heightFector = windowHeight / 768;
                    widthFector = (windowHeight * 1.77864583333) / 1366;
                    fontDiff = (((windowHeight * windowHeight * 1.77) - (1366 * 768)) / font);
                }
                else
                {
                    heightFector = windowWidth / 768;
                    widthFector = windowHeight / 1366;
                    fontDiff = (((windowWidth * windowHeight) - (1366 * 768)) / font);
                }
                transformComponents = true;
            }
            else
            {
                widthFector = windowWidth / 1366;
                heightFector = windowHeight / 768;
                fontDiff = (((windowWidth * windowHeight) - (1366 * 768)) / font);
            }
        }

        public void maintainAppbar(AppBar appbar)
        {
            appbar.Width *= widthFector;
            appbar.Height *= heightFector;
        }

        public void maintainButton(Button button)
        {
            button.Width *= widthFector;
            button.Height *= heightFector;
            button.FontSize += (fontDiff / 2);
            margin = button.Margin;
            setMargin();
            button.Margin = margin;
        }

        public void maintainImage(Image image)
        {
            image.Width *= widthFector;
            image.Height *= heightFector;
            margin = image.Margin;
            setMargin();
            image.Margin = margin;
        }

        public void maintainShape(Shape shape)
        {
            shape.Width *= widthFector;
            shape.Height *= heightFector;
            margin = shape.Margin;
            setMargin();
            shape.Margin = margin;
        }

        public void maintainTextblock(TextBlock textblock)
        {
            textblock.Width *= widthFector;
            textblock.Height *= heightFector;
            textblock.FontSize += fontDiff;
            margin = textblock.Margin;
            setMargin();
            textblock.Margin = margin;
        }

        private void setMargin()
        {
            margin.Top *= heightFector;
            margin.Left *= widthFector;
            margin.Bottom *= heightFector;
            margin.Right *= widthFector;
        }

        public void setButton(Button button, double left_margin, double top_margin)
        {
            margin = button.Margin;
            margin.Left = left_margin;
            margin.Top = top_margin;
            button.Margin = margin;
        }

        public void setImage(Image image, double left_margin, double top_margin)
        {
            margin = image.Margin;
            margin.Left = left_margin;
            margin.Top = top_margin;
            image.Margin = margin;
        }

        public void setShape(Shape shape, double left_margin, double top_margin)
        {
            margin = shape.Margin;
            margin.Left = left_margin;
            margin.Top = top_margin;
            shape.Margin = margin;
        }

        public void setTextBlock(TextBlock textblock, double left_margin, double top_margin)
        {
            margin = textblock.Margin;
            margin.Left = left_margin;
            margin.Top = top_margin;
            textblock.Margin = margin;
        }
    }
}
