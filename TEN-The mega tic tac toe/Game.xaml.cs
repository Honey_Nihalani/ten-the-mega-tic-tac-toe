﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace TEN_The_mega_tic_tac_toe
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Game : Page
    {
        string ellipse_tag;
        bool[, , ,] statuses;
        int[, , ,] whole_game;
        int i, j, k, l, pre_k, pre_l;
        int[,] main_game, tic_tac_toe;
        bool changeTurn, randomMode, gameOver, firstClick = true, sound_on = true;
        double widthFector, heightFector, fector, windowWidth, windowHeight, DecOpacity = 0.2;
        Color[] colors;
        Shape[, , ,] position;
        ComponentMaintainer cm;
        Ellipse[,] player1_circle;
        Rectangle[,] player2_squre, draw_rod;
        Color back_color, player1_color, player2_color;
        MediaElement player1_sound, player2_sound, zone_draw_sound, zone_player_1_sound, zone_player_2_sound, game_over_sound;

        public Game()
        {
            this.InitializeComponent();
            veriableDeclaration();
            cm = new ComponentMaintainer(50000);
            widthFector = cm.widthFector;
            heightFector = cm.heightFector;
            maintainComponent();
            if (cm.transformComponents)
                transformComponents();
            Window.Current.SizeChanged += Current_SizeChanged;
        }

        void veriableDeclaration()
        {
            main_game = new int[3, 3];
            tic_tac_toe = new int[3, 3];
            whole_game = new int[3, 3, 3, 3];
            statuses = new bool[3, 3, 3, 3];
            position = new Shape[3, 3, 3, 3];
            player1_circle = new Ellipse[3, 3];
            player2_squre = new Rectangle[3, 3];
            draw_rod = new Rectangle[3, 3];
            player1_sound = new MediaElement();
            player2_sound = new MediaElement();
            zone_player_1_sound = new MediaElement();
            zone_player_2_sound = new MediaElement();
            zone_draw_sound = new MediaElement();
            game_over_sound = new MediaElement();
            player1_sound.Source = new Uri(@"ms-appx:///sounds/player_1.wav", UriKind.RelativeOrAbsolute);
            player2_sound.Source = new Uri(@"ms-appx:///sounds/player_2.wav", UriKind.RelativeOrAbsolute);
            zone_player_1_sound.Source = new Uri(@"ms-appx:///sounds/zone_player_1.wav", UriKind.RelativeOrAbsolute);
            zone_player_2_sound.Source = new Uri(@"ms-appx:///sounds/zone_player_2.wav", UriKind.RelativeOrAbsolute);
            zone_draw_sound.Source = new Uri(@"ms-appx:///sounds/zone_draw.wav", UriKind.RelativeOrAbsolute);
            game_over_sound.Source = new Uri(@"ms-appx:///sounds/game_over.wav", UriKind.RelativeOrAbsolute);
            player1_sound.AutoPlay = false;
            player2_sound.AutoPlay = false;
            zone_player_1_sound.AutoPlay = false;
            zone_player_2_sound.AutoPlay = false;
            zone_draw_sound.AutoPlay = false;
            game_over_sound.AutoPlay = false;
            myGrid.Children.Add(player1_sound);
            myGrid.Children.Add(player2_sound);
            myGrid.Children.Add(zone_player_1_sound);
            myGrid.Children.Add(zone_player_2_sound);
            myGrid.Children.Add(zone_draw_sound);
            myGrid.Children.Add(game_over_sound);
        }

        void maintainComponent()
        {
            cm.maintainShape(player_status);
            cm.maintainShape(circle_symbol);
            cm.maintainShape(square_symbol);
            cm.maintainTextblock(game_status);
            cm.maintainAppbar(Options);
            cm.maintainButton(home_button);
            cm.maintainButton(restart_button);
            cm.maintainButton(sound_button);
        }

        void Current_SizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            transformComponents();
        }

        void transformComponents()
        {
            windowWidth = Window.Current.Bounds.Width;
            windowHeight = Window.Current.Bounds.Height;

            player_status.Width = windowWidth;
            cm.setTextBlock(game_status, (windowWidth - game_status.Width) / 2, game_status.Margin.Top);
            cm.setShape(circle_symbol, (windowWidth - circle_symbol.Width) / 2, circle_symbol.Margin.Top);
            cm.setShape(square_symbol, (windowWidth - square_symbol.Width) / 2, square_symbol.Margin.Top);
            cm.setButton(restart_button, (windowWidth - restart_button.Width) / 2, restart_button.Margin.Top);
            cm.setButton(home_button, (restart_button.Margin.Left - home_button.Width) / 2, restart_button.Margin.Top);
            cm.setButton(sound_button, restart_button.Margin.Left + restart_button.Width + home_button.Margin.Left, sound_button.Margin.Top);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            colors = e.Parameter as Color[];
            back_color = colors[0];
            player1_color = colors[1];
            player2_color = colors[2];
            myGrid.Background = new SolidColorBrush(back_color);
            player_status.Fill = new SolidColorBrush(player1_color);
            Options.Background = player_status.Fill;
            p1_colour.Value = player1_color;
            p2_colour.Value = player2_color;
            GeneratPositions();
        }

        void GeneratPositions()
        {
            if (widthFector == heightFector)  // it is for 1366 x 768 resolution
                fector = 1;
            else if (widthFector > heightFector)
                fector = widthFector;
            else
                fector = heightFector;

            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 3; j++)
                {
                    player1_circle[i, j] = new Ellipse();
                    player1_circle[i, j].Height = 150 * fector;
                    player1_circle[i, j].Width = 150 * fector;
                    player1_circle[i, j].Fill = new SolidColorBrush(player1_color);
                    player1_circle[i, j].Margin = new Thickness((-410 + (j * 410)) * fector, (-330 + (i * 410)) * fector, 0, 0);
                    player1_circle[i, j].Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    myGrid.Children.Add(player1_circle[i, j]);

                    player2_squre[i, j] = new Rectangle();
                    player2_squre[i, j].Height = 150 * fector;
                    player2_squre[i, j].Width = 150 * fector;
                    player2_squre[i, j].Fill = new SolidColorBrush(player2_color);
                    player2_squre[i, j].Margin = new Thickness((-410 + (j * 410)) * fector, (-330 + (i * 410)) * fector, 0, 0);
                    player2_squre[i, j].Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    myGrid.Children.Add(player2_squre[i, j]);

                    draw_rod[i, j] = new Rectangle();
                    draw_rod[i, j].Height = 50 * fector;
                    draw_rod[i, j].Width = 150 * fector;
                    draw_rod[i, j].Fill = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 255, 255, 255));
                    draw_rod[i, j].Margin = new Thickness((-410 + (j * 410)) * fector, (-330 + (i * 410)) * fector, 0, 0);
                    draw_rod[i, j].Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    myGrid.Children.Add(draw_rod[i, j]);

                    for (k = 0; k < 3; k++)
                    {
                        for (l = 0; l < 3; l++)
                        {
                            position[i, j, k, l] = new Ellipse();
                            position[i, j, k, l].Height = 40 * fector;
                            position[i, j, k, l].Width = 40 * fector;
                            position[i, j, k, l].Fill = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 255, 255, 255));
                            position[i, j, k, l].Margin = new Thickness((-530 + (l * 120) + (j * 410)) * fector, (-450 + (k * 120) + (i * 410)) * fector, 0, 0);
                            position[i, j, k, l].Tag = "" + i + j + k + l;
                            position[i, j, k, l].PointerPressed += Ellipse_pressed;
                            myGrid.Children.Add(position[i, j, k, l]);
                        }
                    }
                }
            }
        }

        void Ellipse_pressed(object sender, PointerRoutedEventArgs e)
        {
            ellipse_tag = ((Ellipse)sender).Tag.ToString();
            i = int.Parse(ellipse_tag.Substring(0, 1));
            j = int.Parse(ellipse_tag.Substring(1, 1));
            k = int.Parse(ellipse_tag.Substring(2, 1));
            l = int.Parse(ellipse_tag.Substring(3, 1));

            if (whole_game[i, j, k, l] == 0 && !gameOver)
            {
                if (firstClick || randomMode)
                {
                    whole_game[i, j, k, l] = 1;
                    DisableAllZones();
                    main_procedure((Shape)sender);
                    if (main_game[k, l] == 0)
                        randomMode = false;
                    firstClick = false;
                    if (!gameOver)
                        changePlayerTurn();
                }
                else if (statuses[i, j, k, l])
                {
                    main_procedure((Shape)sender);
                    if (!gameOver)
                        changePlayerTurn();
                }
            }
        }

        void main_procedure(Shape sender)
        {
            changeTurn = !changeTurn;

            //set players bit in whole_game
            if (changeTurn)
            {
                whole_game[i, j, k, l] = 1;
                sender.Fill = new SolidColorBrush(player1_color);
            }
            else
            {
                whole_game[i, j, k, l] = 2;
                myGrid.Children.Remove(position[i, j, k, l]);
                position[i, j, k, l] = new Rectangle();
                position[i, j, k, l].Height = sender.Height;
                position[i, j, k, l].Width = sender.Width;
                position[i, j, k, l].Opacity = sender.Opacity;
                position[i, j, k, l].Margin = sender.Margin;
                position[i, j, k, l].Fill = new SolidColorBrush(player2_color);
                myGrid.Children.Add(position[i, j, k, l]);
            }

            //prepare mini tic_tac_toe matrix
            for (int a = 0; a < 3; a++)
                for (int b = 0; b < 3; b++)
                    tic_tac_toe[a, b] = whole_game[i, j, a, b];

            //check is there a trayo in mini tic_tac_toe matrix..??
            if (trayoInMatrix(tic_tac_toe, int.Parse(ellipse_tag.Substring(2, 2)), whole_game[i, j, k, l]))
            {
                //set players bit in main_game
                main_game[i, j] = whole_game[i, j, k, l];
                setZoneOfPlayers();
                //now check is there a trayo in main_game matrix..??
                if (trayoInMatrix(main_game, int.Parse(ellipse_tag.Substring(0, 2)), main_game[i, j]))
                {
                    gameOver = true;
                    game_over_sound.Play();
                    if (changeTurn)
                    {
                        //player1_WON
                        game_status.Text = "Circles win!";
                        circle_symbol.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        //player2_WON
                        game_status.Text = "Squares win!";
                        square_symbol.Visibility = Visibility.Collapsed;
                    }
                }
                //now there is no trayo in MAIN_GAME.. so check for tie in MAIN_GAME..
                else if (tieInMatrix(main_game))
                {
                    //show msg game is tied
                    game_status.Text = "It's a tie!";
                    if (changeTurn)
                        circle_symbol.Visibility = Visibility.Collapsed;
                    else
                        square_symbol.Visibility = Visibility.Collapsed;
                    player_status.Fill = new SolidColorBrush(back_color);
                    Options.Background = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));
                    gameOver = true;
                    game_over_sound.Play();
                }
                else
                {
                    procedeToEnableZones();
                    if (changeTurn)
                        zone_player_1_sound.Play();
                    else
                        zone_player_2_sound.Play();
                }
            }
            //now there is no trayo in ZONE.. so check for tie in perticular ZONE..
            else if (tieInMatrix(tic_tac_toe))
            {
                for (int a = 0; a < 3; a++)
                {
                    for (int b = 0; b < 3; b++)
                    {
                        position[i, j, a, b].Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        statuses[i, j, a, b] = false;
                    }
                }
                draw_rod[i, j].Visibility = Windows.UI.Xaml.Visibility.Visible;
                main_game[i, j] = 3;

                //now there is no trayo in MAIN_GAME.. but check for tie in MAIN_GAME.. as 
                if (tieInMatrix(main_game))
                {
                    //show msg game tied
                    player_status.Fill = new SolidColorBrush(back_color);
                    gameOver = true;
                    game_over_sound.Play();
                }
                else
                {
                    procedeToEnableZones();
                    zone_draw_sound.Play();
                }
            }
            else
            {
                if (!firstClick && !randomMode)
                    DisableZone(pre_k, pre_l);
                procedeToEnableZones();
                if (changeTurn)
                    player1_sound.Play();
                else
                    player2_sound.Play();
            }
        }

        void EnableZone(int x, int y)
        {
            for (int a = 0; a < 3; a++)
            {
                for (int b = 0; b < 3; b++)
                {
                    position[x, y, a, b].Opacity = 1;
                    statuses[x, y, a, b] = true;
                }
            }
        }

        void DisableZone(int x, int y)
        {
            for (int a = 0; a < 3; a++)
            {
                for (int b = 0; b < 3; b++)
                {
                    if (whole_game[x, y, a, b] == 0)
                        position[x, y, a, b].Opacity *= DecOpacity;
                    statuses[x, y, a, b] = false;
                }
            }
        }

        void procedeToEnableZones()
        {
            if (main_game[k, l] == 0)
            {
                EnableZone(k, l);
                pre_k = k;
                pre_l = l;
            }
            else
            {
                EnableAllReamainingZones();
                randomMode = true;
            }
        }

        void EnableAllReamainingZones()
        {
            for (int a = 0; a < 3; a++)
            {
                for (int b = 0; b < 3; b++)
                {
                    if (main_game[a, b] == 0)
                        EnableZone(a, b);
                }
            }
        }

        void DisableAllZones()
        {
            for (int a = 0; a < 3; a++)
                for (int b = 0; b < 3; b++)
                    DisableZone(a, b);
        }

        void setZoneOfPlayers()
        {
            for (int a = 0; a < 3; a++)
            {
                for (int b = 0; b < 3; b++)
                {
                    position[i, j, a, b].Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    statuses[i, j, a, b] = false;
                }
            }
            if (changeTurn)
                player1_circle[i, j].Visibility = Windows.UI.Xaml.Visibility.Visible;
            else
                player2_squre[i, j].Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        bool trayoInMatrix(int[,] matrix, int position, int player)
        {
            switch (position)
            {
                case 00: if ((matrix[1, 0] == player && matrix[2, 0] == player) || (matrix[0, 1] == player && matrix[0, 2] == player) || (matrix[1, 1] == player && matrix[2, 2] == player)) return true; break;
                case 01: if ((matrix[0, 0] == player && matrix[0, 2] == player) || (matrix[1, 1] == player && matrix[2, 1] == player)) return true; break;
                case 02: if ((matrix[0, 0] == player && matrix[0, 1] == player) || (matrix[1, 2] == player && matrix[2, 2] == player) || (matrix[1, 1] == player && matrix[2, 0] == player)) return true; break;
                case 10: if ((matrix[0, 0] == player && matrix[2, 0] == player) || (matrix[1, 1] == player && matrix[1, 2] == player)) return true; break;
                case 11: if ((matrix[0, 0] == player && matrix[2, 2] == player) || (matrix[0, 2] == player && matrix[2, 0] == player) || (matrix[0, 1] == player && matrix[2, 1] == player) || (matrix[1, 0] == player && matrix[1, 2] == player)) return true; break;
                case 12: if ((matrix[0, 2] == player && matrix[2, 2] == player) || (matrix[1, 0] == player && matrix[1, 1] == player)) return true; break;
                case 20: if ((matrix[0, 0] == player && matrix[1, 0] == player) || (matrix[2, 1] == player && matrix[2, 2] == player) || (matrix[1, 1] == player && matrix[0, 2] == player)) return true; break;
                case 21: if ((matrix[2, 0] == player && matrix[2, 2] == player) || (matrix[0, 1] == player && matrix[1, 1] == player)) return true; break;
                case 22: if ((matrix[0, 0] == player && matrix[1, 1] == player) || (matrix[0, 2] == player && matrix[1, 2] == player) || (matrix[2, 0] == player && matrix[2, 1] == player)) return true; break;
            }
            return false;
        }

        bool tieInMatrix(int[,] matrix)
        {
            for (int a = 0; a < 3; a++)
            {
                for (int b = 0; b < 3; b++)
                {
                    if (matrix[a, b] == 0)
                        return false;
                }
            }
            return true;
        }

        void changePlayerTurn()
        {
            if (changeTurn)
                circle_symbol.Visibility = Visibility.Collapsed;
            else
                square_symbol.Visibility = Visibility.Collapsed;
            change_player.Begin();
            change_player.Completed += change_player_Completed;
        }

        void change_player_Completed(object sender, object e)
        {
            if (changeTurn)
            {
                square_symbol.Visibility = Visibility.Visible;
                player_status.Fill = new SolidColorBrush(player2_color);
                Options.Background = player_status.Fill;
                p1_colour.Value = player2_color;
                p2_colour.Value = player1_color;
            }
            else
            {
                circle_symbol.Visibility = Visibility.Visible;
                player_status.Fill = new SolidColorBrush(player1_color);
                Options.Background = player_status.Fill;
                p1_colour.Value = player1_color;
                p2_colour.Value = player2_color;
            }
        }

        void option_clicked(object sender, RoutedEventArgs e)
        {
            if (sender.Equals(home_button))
                this.Frame.Navigate(typeof(MainPage));
            else if (sender.Equals(restart_button))
                this.Frame.Navigate(typeof(Game), colors);
            else
            {
                if (sound_on)
                {
                    sound_on = false;
                    player1_sound.IsMuted = true;
                    player2_sound.IsMuted = true;
                    zone_player_1_sound.IsMuted = true;
                    zone_player_2_sound.IsMuted = true;
                    zone_draw_sound.IsMuted = true;
                    game_over_sound.IsMuted = true;
                    sound_button.Content = "Sound ON";
                }
                else
                {
                    sound_on = true;
                    player1_sound.IsMuted = false;
                    player2_sound.IsMuted = false;
                    zone_player_1_sound.IsMuted = false;
                    zone_player_2_sound.IsMuted = false;
                    zone_draw_sound.IsMuted = false;
                    game_over_sound.IsMuted = false;
                    sound_button.Content = "Sound OFF";
                }
            }
        }
    }
}
