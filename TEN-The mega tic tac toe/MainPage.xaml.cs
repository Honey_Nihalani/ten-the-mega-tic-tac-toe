﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace TEN_The_mega_tic_tac_toe
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        object[] data;
        int[] data_theme;
        string upData = "";
        int page_no, theme_no;
        double widthFector, heightFector, windowWidth, windowHeight;
        Color[] colors;
        ComponentMaintainer cm;
        StorageFile user_preferences;

        public MainPage()
        {
            this.InitializeComponent();
            colors = new Color[3];
            data_theme = new int[2];
            cm = new ComponentMaintainer(50000);
            widthFector = cm.widthFector;
            heightFector = cm.heightFector;
            maintainComponent();
            if (cm.transformComponents)
                transformComponents();
            Window.Current.SizeChanged += Current_SizeChanged;
            start_game.Begin();
        }

        void maintainComponent()
        {
            cm.maintainButton(play_game_button);
            cm.maintainButton(theme_button);
            cm.maintainButton(about_game_button);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            data = e.Parameter as object[];
            start_game.Begin();
            try
            {
                page_no = (int)data[0];
                theme_no = (int)data[1];
                colors[0] = (Color)data[2];
                colors[1] = (Color)data[3];
                colors[2] = (Color)data[4];
                myGrid.Background = play_game_button.Foreground = theme_button.Foreground = about_game_button.Foreground = new SolidColorBrush(colors[1]);
            }
            catch (NullReferenceException e1)
            {
                data = new object[5];
                getPreferenceFile();
                start_game.Completed += start_game_Completed;
            }
        }

        void Current_SizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            transformComponents();
        }

        void transformComponents()
        {
            windowWidth = Window.Current.Bounds.Width;
            windowHeight = Window.Current.Bounds.Height;

            cm.setButton(theme_button, (windowWidth - theme_button.Width) / 2, (windowHeight - theme_button.Height) / 2);
            cm.setButton(play_game_button, theme_button.Margin.Left, (windowHeight - (3 * theme_button.Height + 24 * heightFector)) / 2);
            cm.setButton(about_game_button, theme_button.Margin.Left, theme_button.Margin.Top + theme_button.Height + (12 * heightFector));
        }

        async void getPreferenceFile()
        {
            try
            {
                user_preferences = await ApplicationData.Current.LocalFolder.GetFileAsync("preferences.txt");
                upData = await FileIO.ReadTextAsync(user_preferences);
            }
            catch (Exception e)
            {
                creatFile();
            }
        }

        async void creatFile()
        {
            await ApplicationData.Current.LocalFolder.CreateFileAsync("preferences.txt");
            user_preferences = await ApplicationData.Current.LocalFolder.GetFileAsync("preferences.txt");
            string data = 1 + "$$"
                        + 4 + "$$"
                        + 255 + "$" + 60 + "$" + 60 + "$" + 112 + "$$"
                        + 255 + "$" + 69 + "$" + 147 + "$" + 247 + "$$"
                        + 255 + "$" + 249 + "$" + 199 + "$" + 92 + "$$";
            await FileIO.WriteTextAsync(user_preferences, data);
        }

        void readDataFromFile()
        {
            string[,] myData = new string[5, 4];
            int i, j, k;
            for (i = 0; i < 5; i++)
                for (j = 0; j < 4; j++)
                    myData[i, j] = "";
            i = 0;
            j = 0;
            k = 0;
            char[] arr = upData.ToCharArray();
            while (i < arr.Length)
            {
                if (arr[i] == '$')
                {
                    if (arr[i + 1] == '$')
                    {
                        j++;
                        i++;
                        k = 0;
                    }
                    else
                        k++;
                    i++;
                }
                try
                {
                    myData[j, k] += arr[i];
                    i++;
                }
                catch (IndexOutOfRangeException e)
                {

                }
            }
            try
            {
                page_no = int.Parse(myData[0, 0]);
                theme_no = int.Parse(myData[1, 0]);
                colors[0] = Color.FromArgb(byte.Parse(myData[2, 0]), byte.Parse(myData[2, 1]), byte.Parse(myData[2, 2]), byte.Parse(myData[2, 3]));
                colors[1] = Color.FromArgb(byte.Parse(myData[3, 0]), byte.Parse(myData[3, 1]), byte.Parse(myData[3, 2]), byte.Parse(myData[3, 3]));
                colors[2] = Color.FromArgb(byte.Parse(myData[4, 0]), byte.Parse(myData[4, 1]), byte.Parse(myData[4, 2]), byte.Parse(myData[4, 3]));
            }
            catch (Exception e)
            {
                page_no = 1;
                theme_no = 4;
                colors[0] = Color.FromArgb(255, 60, 60, 112);
                colors[1] = Color.FromArgb(255, 69, 147, 247);
                colors[2] = Color.FromArgb(255, 249, 199, 92);
            }
            data[0] = page_no;
            data[1] = theme_no;
            data[2] = colors[0];
            data[3] = colors[1];
            data[4] = colors[2];
            myGrid.Background = play_game_button.Foreground = theme_button.Foreground = about_game_button.Foreground = new SolidColorBrush(colors[1]);
        }

        void start_game_Completed(object sender, object e)
        {
            readDataFromFile();
        }

        void button_clicked(object sender, RoutedEventArgs e)
        {
            if (sender.Equals(play_game_button))
                this.Frame.Navigate(typeof(Game), colors);
            else if (sender.Equals(theme_button))
            {
                data_theme[0] = page_no;
                data_theme[1] = theme_no;
                this.Frame.Navigate(typeof(Theme), data_theme);
            }
            else
                this.Frame.Navigate(typeof(AboutGame), colors);
        }
    }
}
