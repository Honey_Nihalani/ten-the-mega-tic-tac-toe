﻿

#pragma checksum "D:\projects\windows store\TEN-The mega tic tac toe\TEN-The mega tic tac toe\Theme.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "4FDE79F5AD72F3B9042075CB80ECA701"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TEN_The_mega_tic_tac_toe
{
    partial class Theme : global::Windows.UI.Xaml.Controls.Page
    {
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Media.Animation.Storyboard change_color; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Media.Animation.EasingColorKeyFrame old_color; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Media.Animation.EasingColorKeyFrame new_color; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Grid myGrid; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Shapes.Rectangle title_back; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Shapes.Rectangle theme_back; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.TextBlock title; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Image pre_page; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Image next_page; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Image back_button; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private bool _contentLoaded;

        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent()
        {
            if (_contentLoaded)
                return;

            _contentLoaded = true;
            global::Windows.UI.Xaml.Application.LoadComponent(this, new global::System.Uri("ms-appx:///Theme.xaml"), global::Windows.UI.Xaml.Controls.Primitives.ComponentResourceLocation.Application);
 
            change_color = (global::Windows.UI.Xaml.Media.Animation.Storyboard)this.FindName("change_color");
            old_color = (global::Windows.UI.Xaml.Media.Animation.EasingColorKeyFrame)this.FindName("old_color");
            new_color = (global::Windows.UI.Xaml.Media.Animation.EasingColorKeyFrame)this.FindName("new_color");
            myGrid = (global::Windows.UI.Xaml.Controls.Grid)this.FindName("myGrid");
            title_back = (global::Windows.UI.Xaml.Shapes.Rectangle)this.FindName("title_back");
            theme_back = (global::Windows.UI.Xaml.Shapes.Rectangle)this.FindName("theme_back");
            title = (global::Windows.UI.Xaml.Controls.TextBlock)this.FindName("title");
            pre_page = (global::Windows.UI.Xaml.Controls.Image)this.FindName("pre_page");
            next_page = (global::Windows.UI.Xaml.Controls.Image)this.FindName("next_page");
            back_button = (global::Windows.UI.Xaml.Controls.Image)this.FindName("back_button");
        }
    }
}



